#ifndef PRODUTOS_HPP
#define PRODUTOS_HPP
#include"funcoes.hpp"

class produtos
{
    private:
        string nome,categoria;
        float preco;
        int quantidade;
    
    public:
        produtos();
        produtos(string nome,int quantidade,float preco,string categoria);
        ~produtos();
        //metodos acessores
        void set_nome(string nome);
        string get_nome();
        void set_categoria(string categoria);
        string get_categoria();
        void set_preco(float preco);
        float get_preco();
        void set_quantidade(int quantidade);
        int get_quantidade();
        //outros metodos 
        void imprimi();
        friend ostream &operator <<(ostream &out, const produtos &obj);
        friend istream &operator >>(istream &in, produtos &obj);
};

#endif