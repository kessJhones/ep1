#ifndef MENU_HPP
#define MENU_HPP
#include"funcoes.hpp"
#include"venda.hpp"


class menu
{
    virtual void abstract() = 0;
    
    public:
        static void menu_principal();
        static void venda();
        static void cadastro();
        static void recomenda();
        static void estoque();
        static int sub_menu_altera();
};

#endif