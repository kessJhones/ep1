#ifndef CADASTRO_HPP
#define CADASTRO_HPP
#include"funcoes.hpp"

class cadastro
{
    public:
        //metodos
        static void cria_lista_clientes();
        static void cadastra();
        static void lista();
        static void altera();
        static void verifica_cliente(long int cpf);  
        static void altera_socio();
        static bool verifica_socio(long int cpf);
        static void verifica_cpf(long int cpf);  

        

};
#endif