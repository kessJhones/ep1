#ifndef FUNCOES_HPP
#define FUNCOES_HPP

#include"cliente.hpp"
#include"produtos.hpp"
#include"estoque.hpp"
#include"menu.hpp"
#include"cadastro.hpp"
#include"venda.hpp"
#include<iostream>
#include<string>
#include<fstream>
#include<vector>
using namespace std;

extern vector<cliente> lista_de_clientes;
extern vector<produtos> lista_de_produtos;

string getString();
int getInt();
long getLong();
float getFloat();

#endif