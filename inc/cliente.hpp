#include<iostream>
#include<string>
#include<fstream>
#include<vector>
using namespace std;

class cliente
{
    private:
    //Atributos
    string nome;
    long int cpf;
    string telefone;
    string email;

    public:
    //métodos
    cliente();
    cliente(string nome,long int cpf,string email,string telefone);//construtor 
    ~cliente();//Destrutor

    //Métodos acessores
    void set_nome(string nome);
    string get_nome();
    void set_cpf(long int cpf);
    long int get_cpf();
    void set_email(string email);
    string get_email();
    void set_telefone(string telefone);
    string get_telefone();

    //Outros Métodos
    void imprimi_dados();
    friend ostream &operator <<(ostream &out, const cliente &obj);
    friend istream &operator >>(istream &in, cliente &obj);
}; 