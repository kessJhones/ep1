#ifndef VENDA_HPP
#define VENDA_HPP
#include"funcoes.hpp"

class venda
{
    private:
        float total,desconto,valor_final;
        string categorias;
        long int cpf;

    public:
        //construtores e destrutor
        venda(long int cpf);
        venda(float total,float desconto);
        ~venda();
        //acessores
        void set_total(float total);
        float get_total();
        void set_desconto(float desconto);
        float get_desconto();
        void set_valor_final(float total,float desconto);
        float get_valor_final();
        void set_cpf(long int cpf);
        long int get_cpf();
        //Outros
        void imprime();
        public:
        static void carrinho();
};

#endif