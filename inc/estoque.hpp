#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP
#include"funcoes.hpp"

class estoque
{
    public:
        static void cria_lista_produtos();
        static void cadastra();
        static void atualiza_quantidade();
        static void atualiza_quantidade(int quantidade,int indice);
        static void lista();
        static bool verifica_produto(string nome);
};

#endif