#include"cadastro.hpp"

vector<cliente> lista_de_clientes;

void cadastro::cria_lista_clientes()
{
    cliente aux;
    ifstream arq("cliente.txt",ios::app);

    while(!arq.eof())
    {
        arq>>aux;
        lista_de_clientes.push_back(aux);
    }
    if(lista_de_clientes.size()>1)
        lista_de_clientes.pop_back();
}

void cadastro::cadastra()
{
    string nome,email,telefone;
    long int cpf;
    string socio;

    cout<<"Nome do cliente:"<<endl;
    nome = getString();
    cout<<"CPF:"<<endl;
    cpf = getLong();
    cadastro::verifica_cpf(cpf);
    cout<<"Email:"<<endl;
    email = getString();
    cout<<"Telefone:"<<endl;
    telefone = getString();

    cout<<"Digite sim, se o cliente for socio\nQualquer tecla se não"<<endl;
    socio = getString();
    if(socio=="sim"){
        ofstream abre("socio.txt",ios::app);
        abre<<cpf<<endl;
        abre.close();
    }

    cliente aux = cliente(nome,cpf,email,telefone);

    lista_de_clientes.push_back(aux);

    ofstream aArquivo("cliente.txt",ios::app);
    aArquivo<<aux;
    aArquivo.close();
    
    menu::cadastro();
}

void cadastro::lista()
{
    int i;

    for(i=0;i<(int)lista_de_clientes.size();i++){
        lista_de_clientes[i].imprimi_dados();
    }

    menu::cadastro();
}

void cadastro::altera()
{
    cliente aux;
    int i,opcao;
    long int cpf;

    cout<<"Digite o cpf a ser alterado";
    cpf = getLong();

    for(i=0;i<(int)lista_de_clientes.size();i++)
    {
        
        if(cpf==lista_de_clientes[i].get_cpf())
        {
            opcao = menu::sub_menu_altera();
            switch (opcao)
            {
            case 1:
                {
                    string nome;
                    nome = getString();
                    lista_de_clientes[i].set_nome(nome);
                }
                break;
            
            default:
                break;
            }
            break;
        }    
    }

    ofstream aArquivo("cliente.txt");
    for(i=0;i<(int)lista_de_clientes.size();i++){
        aArquivo<<lista_de_clientes[i];
    }
    aArquivo.close();
    menu::cadastro();
}

void cadastro::verifica_cliente(long int cpf)
{
    int i;
    bool controle;
    for(i=0;i<(int)lista_de_clientes.size();i++)
    {
        if(lista_de_clientes[i].get_cpf()==cpf){
            controle = true;
        }
    }

    if(controle){
    }
    else{
        cout<<"Usuario não cadastrado"<<endl;
        cout<<"1 - Cadastrar "<<endl;
        cout<<"2 - Voltar para o menu principal"<<endl;
        int controle = getInt();
        if(controle==1)
            cadastro::cadastra();
        else{
            menu::menu_principal();
        }
    }
}

bool cadastro::verifica_socio(long int cpf)
{
    string aux;

    ifstream aArquivo("socio.txt");
    while(getline(aArquivo,aux)){
        if(atoi(aux.c_str())==cpf)
            return true;
    }
    return false;
}


void cadastro::verifica_cpf(long int cpf)
{
    int i;
    bool controle;
    for(i=0;i<(int)lista_de_clientes.size();i++)
    {
        if(lista_de_clientes[i].get_cpf()==cpf){
            controle = true;
        }
    }

    if(controle){
        cout<<"CPF já cadastrado"<<endl;
        menu::cadastro();
    }
}