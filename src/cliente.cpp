#include<iostream>
#include<string>
#include<fstream>
#include<vector>
#include"cliente.hpp"

using namespace std;

cliente::cliente(string nome, long int cpf,string email,string telefone)
{
    set_nome(nome);
    set_cpf(cpf);
    set_email(email);
    set_telefone(telefone);
}

cliente::~cliente(){
}

cliente::cliente(){
}

void cliente::set_nome(string nome){
    int i;
    for(i=0;i<(int)nome.size();i++)
    {
        if(nome[i]==' '){
            nome[i]='_';
        }
    }
    this->nome = nome;
}

string cliente::get_nome(){
    return nome;
}

void cliente::set_cpf(long int cpf){
    this->cpf = cpf;
}

long int cliente::get_cpf(){
    return cpf;
}

void cliente::set_email(string email){
    this->email = email;
}

string cliente::get_email(){
    return email;
}

void cliente::set_telefone(string telefone){
    this->telefone = telefone;
}

string cliente::get_telefone(){
    return telefone;
}

//Outros metodos
void cliente::imprimi_dados()
{
    cout<<"Nome: "<<nome<<endl;
    cout<<"Cpf: "<<cpf<<endl;
    cout<<"Telefone: "<<telefone<<endl;
    cout<<"Email: "<<email<<endl;
}

ostream &operator <<(ostream &out, const cliente &obj){
	out << obj.nome << "\t" << obj.cpf<<"\t"<<obj.email<<"\t"<<obj.telefone<<endl;
	return out;
}

istream &operator >>(istream &in, cliente &obj){
	in >> obj.nome;
	in >> obj.cpf;
    in >> obj.email;
    in >> obj.telefone;
	return in;
}
