#include"funcoes.hpp"

vector<produtos> lista_de_produtos;

void estoque::cria_lista_produtos()
{
    produtos aux;
    ifstream abre_arquivo("estoque.txt",ios::app);
    
    while(!abre_arquivo.eof()){
        abre_arquivo>>aux;
        lista_de_produtos.push_back(aux);
    }

    if((int)lista_de_produtos.size()>1)
       lista_de_produtos.pop_back();
}

void estoque::cadastra()
{
    string nome,categoria="",auxiliar,email,telefone;
    int qtd,i;
    float preco;

    cout<<"Nome:"<<endl;
    nome = getString();
    cout<<"Quantidade:"<<endl;
    qtd = getInt();
    cout<<"Preço:"<<endl;
    preco = getFloat();
    cout<<"Quantidade de catégorias do produto"<<endl;
    i = getInt();
    cout<<"Categoria(s):"<<endl;
    for(;i>0;i--){
        auxiliar = getString();
        categoria+=" "+auxiliar;
    }

    produtos aux = produtos(nome,qtd,preco,categoria);

    lista_de_produtos.push_back(aux);

    ofstream abre_arquivo("estoque.txt",ios::app);
    abre_arquivo<<aux;
    abre_arquivo.close();
    
    menu::estoque();
}

void estoque::lista()
{
    int i;
    string aux;

    for(i=0;i<(int)lista_de_produtos.size();i++){
        cout<<"\t"<<"Produto"<< i+1<<endl;
        lista_de_produtos[i].imprimi();
    }
}

void estoque::atualiza_quantidade()
{
    string nome;
    int quantidade,i=0;
    string pausa;
    produtos aux;
    vector<produtos> lista;

    cout<<"Digite o Nome do produto: "<<endl;
    nome = getString();
    if(estoque::verifica_produto(nome)){
        cout<<"Digite a nova quantidade: "<<endl;
        quantidade = getInt();
    }
    else
    {
        cout<<"Produto não cadastrado"<<endl;
        cout<<"Aperte qualquer tecla para continuar:"<<endl;
        pausa = getString();
        menu::estoque();
    }

    while(nome!=lista_de_produtos[i].get_nome()){
        i++;
    }

    lista_de_produtos[i].set_quantidade(quantidade);
    
    ofstream grava_arquivo("estoque.txt");
    for(i=0;i<(int)lista_de_produtos.size();i++){
        grava_arquivo<<lista_de_produtos[i];
    }
    grava_arquivo.close();

    menu::estoque();
}

bool estoque::verifica_produto(string nome)
{
    int i;
    for(i=0;i<(int)lista_de_produtos.size();i++)
    {   
        if(nome==lista_de_produtos[i].get_nome())
            return true;
    }
    return false;
}

void estoque::atualiza_quantidade(int quantidade,int indici){
    int aux,i;
    aux = lista_de_produtos[indici].get_quantidade();
    lista_de_produtos[indici].set_quantidade(aux-quantidade);

    ofstream grava_arquivo("estoque.txt");
    for(i=0;i<(int)lista_de_produtos.size();i++){
        grava_arquivo<<lista_de_produtos[i];
    }
    grava_arquivo.close();
}