#include"funcoes.hpp"

venda::venda(long int cpf){
    set_cpf(cpf);
}

venda::~venda(){

}
void venda::set_total(float total){
    this->total = total; 
}

float venda::get_total(){
    return total;
}
void venda::set_desconto(float desconto){
    this->desconto = desconto;
}
float venda::get_desconto(){
    return desconto;
}
void venda::set_valor_final(float total,float desconto){
    this->valor_final = total-desconto;
}
float venda::get_valor_final(){
    return valor_final;
}
void venda::set_cpf(long int cpf){
    this->cpf = cpf;
}
long int venda::get_cpf(){
    return cpf;
}

void venda::imprime(){
    cout<<"Valor total da compra:"<<total<<endl;
    cout<<"Valor do desconto:"<<desconto<<endl;
    cout<<"Valor final da compra:"<<valor_final<<endl;
}

void venda::carrinho()
{
    long int cpf,qtd;
    int opcao;
    string para;
    float total=0;
    vector<produtos> selecionados;

    cout<<"Informe o CPF do cliente:"<<endl;
    cpf=getLong();
    cadastro::verifica_cliente(cpf);
    venda compra = venda(cpf);

    do{
        estoque::lista();
        cout<<"Escolha o produto pelo indice:"<<endl;
        opcao = getInt();
        if((opcao-1)>(int)lista_de_produtos.size()){
            cout<<"Opcao invalidade"<<endl;
        }
        else{
            selecionados.push_back(lista_de_produtos[opcao-1]);
            cout<<"Digite a quantidade"<<endl;
            qtd = getInt();
        }

        if(qtd>lista_de_produtos[opcao-1].get_quantidade() || qtd<=0){
            cout<<"Quantidade superior ao estoque\n Precione qualquer tecla para reiniciar a compra"<<endl;
            selecionados.clear();
            para  = getString();
            menu::venda();
        }
        else
        {
            estoque::atualiza_quantidade(qtd,opcao-1);
            total+=lista_de_produtos[opcao-1].get_preco();
            cout<<"Digite Sim para continuar comprando. Nao para sair."<<endl;
            para = getString();
        }
    }while(para=="sim" || para=="Sim");

    compra.set_total(total);
    compra.set_desconto(0);
    compra.set_valor_final(total,0);
    compra.imprime();
    
    cout<<"Precione qualquer tecla:"<<endl;
    para = getString();

    menu::menu_principal();
}