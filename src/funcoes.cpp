#include<funcoes.hpp>

string getString(){
    while(true){
        string valor;
        try{
            getline(cin, valor);    
        }
        catch(const std::ios_base::failure& e){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida - " << e.what() << " - Insira novamente: " << endl;
        }
        return valor;
    }
}

int getInt(){
    while(true){
        int valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";            
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}

long getLong(){
    while(true){
        long valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";            
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}

float getFloat(){
    while(true){
        float valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";            
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}