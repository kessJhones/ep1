#include"funcoes.hpp"

void menu::menu_principal()
{

    int controle;
    system("clear || cls");
    cout<<"************ Menu ************"<<endl;
    cout<<"** 1-Venda                  **"<<endl;
    cout<<"** 2-Estoque                **"<<endl;
    cout<<"** 3-Cadastro               **"<<endl;
    cout<<"** 0-Sair                   **"<<endl;
    cout<<"******************************"<<endl;
    controle = getInt();

    switch (controle)
    {
        case 0:
            cout<<"Arreverdeste"<<endl;
            exit(0);
            break;
        case 1:
            menu::venda();
            break;
        case 2:
            menu::estoque();
            break;
        case 3:
            menu::cadastro();
            break;
        default:
            cout<<"Opção invalida"<<endl;
            menu::menu_principal();
            break;
    }
}

void menu::cadastro()
{
    int controle;
    system("clear || cls");
    cout<<"************ Cadastro ************"<<endl;
    cout<<"** 1 - Cadastrar cliente        **"<<endl;
    cout<<"** 2 - Alterar cadastro         **"<<endl;
    cout<<"** 3 - Listar Clientes          **"<<endl;
    cout<<"** 0 - Voltar menu principal    **"<<endl;
    cout<<"**********************************"<<endl;
    controle = getInt();

    switch (controle)
    {
        case 1:
            cadastro::cadastra();
            break;
        case 2:
            cadastro::altera();
            break;
        case 3:
            cadastro::lista();
            break;
        case 0:
            menu::menu_principal();
        default:
            cout<<"Opção invalida"<<endl;
            menu::cadastro();
    }
}

void menu::estoque()
{
    int controle;
    system("clear || cls");
    cout<<"************ Estoque *************"<<endl;
    cout<<"** 1 - Cadastrar produto        **"<<endl;
    cout<<"** 2 - Atualizar quantidade     **"<<endl;
    cout<<"** 0 - Voltar menu principal    **"<<endl;
    cout<<"**********************************"<<endl;
    controle = getInt();

    switch (controle)
    {
        case 1:
            estoque::cadastra();
            break;
        case 2:
            estoque::atualiza_quantidade();
            break;
        case 0:
            menu::menu_principal();
        default:
            cout<<"Opção invalida"<<endl;
            menu::estoque();
    }
}

int menu::sub_menu_altera()
{
    int controle;
    system("clear || cls");
    cout<<"*********  Altera cliente  *******"<<endl;
    cout<<"** 1 - Alterar nome             **"<<endl;
    cout<<"** 2 - Alterar email            **"<<endl;
    cout<<"** 2 - Alterar Telefone         **"<<endl;
    cout<<"** 3 - Alterar status de socio  **"<<endl;
    cout<<"** 0 - Voltar menu principal    **"<<endl;
    cout<<"**********************************"<<endl;
    controle = getInt();
    return controle;
}

void menu::venda()
{
    system("clear || cls");
    venda::carrinho();
}