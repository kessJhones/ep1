#include"funcoes.hpp"

produtos::produtos(){
}

produtos::produtos(string nome,int quantidade,float preco,string categoria)
{
    set_nome(nome);
    set_categoria(categoria);
    set_preco(preco);
    set_quantidade(quantidade);
}

produtos::~produtos(){
}

void produtos::set_nome(string nome){
    this->nome = nome;
}

string produtos::get_nome(){
    return nome;
}

void produtos::set_categoria(string categoria){
    this->categoria = categoria;
}

string produtos::get_categoria(){
    return categoria;
}

void produtos::set_preco(float preco){
    this->preco=preco;
}

float produtos::get_preco(){
    return preco;
}

void produtos::set_quantidade(int quantidade){
    this->quantidade=quantidade;
}

int produtos::get_quantidade(){
    return quantidade;
}

//outros metodos 
void produtos::imprimi()
{
    cout<<"Nome:"<<nome<<endl;
    cout<<"Quantidade:"<<quantidade<<endl;
    cout<<"R$:"<<preco<<endl;
}

ostream &operator <<(ostream &out, const produtos &obj){
	out <<obj.nome << "\t" <<obj.quantidade << "\t" <<obj.preco<<obj.categoria<<endl;
	return out;
}

istream &operator >>(istream &in, produtos &obj){
	in >> obj.nome;
	in >> obj.quantidade;
    in >> obj.preco;
    in >> obj.categoria;

	return in;
}